﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace iOSNotesEnexExport
{
    [XmlRoot("en-export")]
    public class DataExport
    {
        [XmlElement("en-export")]
        public string EnExport
        { get; set; }

        private string export_date;
        private string application;
        private string version;

        [XmlAttribute("export-date")]
        public string ExportDate
        {
            get { return export_date; }
            set { export_date = value; }
        }

        [XmlAttribute("application")]
        public string Application
        {
            get { return application; }
            set { application = value; }
        }

        [XmlAttribute("version")]
        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        [XmlElement("note")]
        public List<Note> notes
        { get; set; }
    }

    
    public class Note
    {
        [XmlElement("title")]
        public string Title
        { get; set; }
        [XmlIgnore]
        public string Content
        { get; set; }
        [XmlElement("content")]
        public System.Xml.XmlCDataSection MyStringCDATA
        {
            get
            {
                return new System.Xml.XmlDocument().CreateCDataSection(Content);
            }
            set
            {
                Content = value.Value;
            }
        }
        [XmlElement("created")]
        public string Created
        { get; set; }
        [XmlElement("updated")]
        public string Modified
        { get; set; }
        [XmlElement("note-attributes")]
        public NoteAttributes NoteAttributes
        { get; set; }
    }

    public class NoteAttributes
    {
        [XmlElement("subject-date")]
        public string SubjectDate
        { get; set; }
        [XmlElement("author")]
        public string Author
        { get; set; }
        [XmlElement("source")]
        public string Source
        { get; set; }
        [XmlElement("source-application")]
        public string SourceApplication
        { get; set; }
        [XmlElement("latitude")]
        public string Latitude
        { get; set; }
        [XmlElement("longitude")]
        public string Longitude
        { get; set; }
        [XmlElement("altitude")]
        public string Altitude
        { get; set; }
    }
}

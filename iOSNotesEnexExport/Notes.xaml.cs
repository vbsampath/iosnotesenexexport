﻿using System;
using System.Windows;
using System.Data;

namespace iOSNotesEnexExport
{
    /// <summary>
    /// Interaction logic for Notes.xaml
    /// </summary>
    public partial class Notes : Window
    {
        public NoteManager noteManager;
        public DataTable notes { get; set; }

        public Notes(NoteManager noteManager)
        {
            InitializeComponent();
            this.noteManager = noteManager;
            this.notes = this.noteManager.getNotes();
            this.noteManager.notes = this.notes;
            this.notesDataGrid.DataContext = this;
        }

        private void Process_button_Click(object sender, RoutedEventArgs e)
        {
            Export export = new Export(this.noteManager);
            export.ShowDialog();
        }

        private void Back_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

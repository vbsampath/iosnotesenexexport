﻿using System;
using System.Xml;
using System.Windows;
using System.Collections.Generic;
using System.Collections;
using HtmlAgilityPack;
using System.IO;
using System.Xml.Linq;
using System.Text;

namespace iOSNotesEnexExport
{
    class Utils
    {
        public string getDateFromTimestamp(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime.ToString(Constants.Static_DateFormat);
        }

        public string removeHtmlTags(string inputHtml)
        {
            if (inputHtml.Contains("<html>"))
            {
                //remove html tag
                inputHtml = inputHtml.Replace("<html>", "");
                inputHtml = inputHtml.Replace("</html>", "");
                //remove head tag
                inputHtml = inputHtml.Replace("<head>", "");
                inputHtml = inputHtml.Replace("</head>", "");
                //remove body tag
                inputHtml = inputHtml.Replace("<body>", "");
                inputHtml = inputHtml.Replace("</body>", "");
            }
            inputHtml = inputHtml.Replace("<br>", "<br></br>");
            inputHtml = inputHtml.Replace("<hr>", "<hr></hr>");
            
            return inputHtml;
        }

        
        public string getCDATAString(string content)
        {
            XmlDocument xmlDoc = null;
            try
            {
                xmlDoc = new XmlDocument();
                xmlDoc.XmlResolver = null;   //doesn't break when loading dtd's from external sources
                XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                xmlDoc.AppendChild(docNode);
                XmlNode doctypeNode = xmlDoc.CreateDocumentType("en-note", "SYSTEM", "http://xml.evernote.com/pub/enml2.dtd", null);
                xmlDoc.AppendChild(doctypeNode);
                XmlNode rootNode = xmlDoc.CreateElement("en-note");
                XmlAttribute attribute = xmlDoc.CreateAttribute("style");
                attribute.Value = @"word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;";
                rootNode.Attributes.Append(attribute);
                rootNode.InnerText = content;
                xmlDoc.AppendChild(rootNode);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
            return System.Net.WebUtility.HtmlDecode(xmlDoc.OuterXml);
        }

        public string RemoveNotAllowedAttributes(HtmlDocument html)
        {
            List<string> NotAllowedAttributes = new List<string>(new string[]
	        {
	            "id","class","onclick","ondblclick","on*","accesskey","data","dynsrc","tabindex","contenteditable","height"
	        });
            try
            {

                var elements = html.DocumentNode.SelectNodes("//*");
                if (elements != null)
                {
                    foreach (var element in elements)
                    {
                        var attributeCollection = element.Attributes;
                        List<string> itemsToRemove = new List<string>();
                        if (attributeCollection.Count > 0)
                        {
                            foreach (var attribute in attributeCollection)
                            {
                                if (NotAllowedAttributes.Contains(attribute.Name))
                                {
                                    itemsToRemove.Add(attribute.Name);
                                }
                            }
                            if (itemsToRemove.Count > 0)
                            {
                                foreach (var item in itemsToRemove)
                                {
                                    attributeCollection.Remove(item);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            string content = html.DocumentNode.InnerHtml;
            return content;
        }

        public HtmlDocument HtmlToXElement(string html)
        {
            if (html == null)
                throw new ArgumentNullException("html");

            HtmlDocument doc = new HtmlDocument();
            HtmlNode.ElementsFlags.Remove("img");
            //doc.OptionWriteEmptyNodes = true;
            doc.OptionOutputAsXml = true;
            doc.LoadHtml(html);

            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            
            using (XmlWriter xWriter = XmlWriter.Create(sb, settings))
            {
                doc.Save(xWriter);
                return doc;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.ComponentModel;
using System.Windows;
using System.Xml.Linq;
using HtmlAgilityPack;

namespace iOSNotesEnexExport
{
    public class NoteManager : IDisposable
    {
        #region Properties
        public string notesDbPath
        { get; set; }
        public string notesPath
        { get; set; }
        public DataExport dataExport
        { get; set; }
        public DataTable notes
        { get; set; }
        private XmlSerializer serializer = null;
        private StreamWriter sw = null;
        private XmlWriterSettings settings = null;
        private XmlWriter xw = null;
        private DataTable notesDT = null;
        private SQLiteUtility sqliteUtility = null;
        private Utils utils;
        private bool disposed = false; // to detect redundant calls
        #endregion

        public NoteManager()
        {
            SetupExportEnex();
        }

        public void SetupExportEnex()
        {
            //initializing en-export
            this.dataExport = new DataExport();
            this.dataExport.Version = Constants.Static_Version;
            this.dataExport.Application = Constants.Static_Application;
            this.dataExport.ExportDate = DateTime.Now.ToString(Constants.Static_DateFormat);
        }


        #region Methods
        public DataTable getNotes()
        {
            try
            {
                sqliteUtility = new SQLiteUtility();
                sqliteUtility.notesPath = this.notesDbPath;
                notesDT = sqliteUtility.getNotes();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
            return notesDT;
        }

        public void generateNotes(object sender, DoWorkEventArgs e)
        {
            //initializing notes
            this.dataExport.notes = new List<Note>();
            utils = new Utils();

            int count =0;
            int totalRows = this.notes.Rows.Count;
            try
            {
                foreach (DataRow row in this.notes.Rows)
                {
                    count++;
                    string title = row["ZTITLE"].ToString();
                    
                    string content = utils.removeHtmlTags(row["ZCONTENT"].ToString());
                    HtmlDocument htmlDocument = utils.HtmlToXElement(content);
                    string cleanedContent = utils.RemoveNotAllowedAttributes(htmlDocument);
                    double created = Convert.ToDouble(row["UNIXCREATION"]);
                    double modified = Convert.ToDouble(row["UNIXMODIFICATION"]);
                    string createdDateTime = utils.getDateFromTimestamp(created);
                    string modifiedDateTime = utils.getDateFromTimestamp(modified);
                    object author = row["ZAUTHOR"];

                    string cdataSection = utils.getCDATAString(cleanedContent);

                    Note note = new Note();
                    note.Title = title;
                    note.Content = cdataSection;
                    note.Created = createdDateTime;
                    note.Modified = modifiedDateTime;

                    if (author != DBNull.Value)
                    {
                        NoteAttributes noteAttributes = new NoteAttributes();
                        noteAttributes.Author = author.ToString();
                        noteAttributes.Source = "mail.smtp";
                        note.NoteAttributes = noteAttributes;
                    }
                    else
                    {
                        NoteAttributes noteAttributes = new NoteAttributes();
                        noteAttributes.Source = "mobile.ios";
                        note.NoteAttributes = noteAttributes;
                    }
                    this.dataExport.notes.Add(note);
                    int percentComplete = (int)Math.Round((double)(100 * count) / totalRows);
                    (sender as BackgroundWorker).ReportProgress(percentComplete);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                System.Windows.MessageBox.Show("Error", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void generateEnex()
        {
            try
            {
                serializer = new XmlSerializer(typeof(DataExport));
                sw = new StreamWriter(this.notesPath);
                settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.NewLineOnAttributes = true;           
                xw = XmlTextWriter.Create(sw, settings);
                xw.WriteStartDocument();
                xw.WriteDocType("en-export", null, "http://xml.evernote.com/pub/enml2.dtd", null);
                //xw.WriteDocType("en-export", null, null, "<!ENTITY &nbsp; \"&#160;\">");
                //http://stackoverflow.com/questions/9126999/how-to-handle-html-entity-nbsp-in-xslt-without-changing-the-input-file

                serializer.Serialize(xw, this.dataExport);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                System.Windows.MessageBox.Show("Error", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }
        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (xw != null)
                    {
                        xw.Close();
                    }
                    if (sw != null)
                    {
                        sw.Dispose();
                    }
                    if (sqliteUtility != null)
                    {
                        sqliteUtility.Dispose();
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


﻿using System;
using System.Data;
using Finisar.SQLite;

namespace iOSNotesEnexExport
{
    class SQLiteUtility : IDisposable
    {
        private DataTable DT = null;
        private DataSet DS = null;
        private SQLiteCommand cmd = null;
        private SQLiteDataReader rdr = null;
        private SQLiteDataAdapter DB = null;
        public string notesPath { get; set; }
        private bool disposed = false; // to detect redundant calls

        public string getConnectionString()
        {
            string connectionString = "Data Source=" + this.notesPath + ";Version=3;New=False;Compress=True;";
            return connectionString;
        }


        public DataTable getNotes()
        {
            string notesSQLiteQuery = Constants.Static_NotesQuery;
            
            using (SQLiteConnection conn = new SQLiteConnection(this.getConnectionString()))
            {
                cmd = new SQLiteCommand(notesSQLiteQuery, conn);
                this.DS = new DataSet();
                this.DT = new DataTable();

                try
                {
                    conn.Open();
                    DB = new SQLiteDataAdapter(notesSQLiteQuery, conn);
                    DS.Reset();
                    DB.Fill(DS);
                    DT = DS.Tables[0];
                }
                catch (SQLiteException sqe)
                {
                    Console.WriteLine(sqe.Message);
                }
            }
            return DT;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                    if (DT != null)
                    {
                        DT.Dispose();
                    }
                    if (DS != null)
                    {
                        DS.Dispose();
                    }
                    if (DB != null)
                    {
                        DB.Dispose();
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

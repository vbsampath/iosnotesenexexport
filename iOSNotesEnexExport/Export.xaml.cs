﻿using System;
using System.Windows;
using System.ComponentModel;

namespace iOSNotesEnexExport
{
    /// <summary>
    /// Interaction logic for Export.xaml
    /// </summary>
    public partial class Export : Window
    {
        private NoteManager noteManager { get; set; }
        private DataExport dataExport { get; set; }
        private string fileName { get; set; }

        public Export(NoteManager noteManager)
        {
            InitializeComponent();
            this.noteManager = noteManager;
        }

        private void Browse_button_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "notes"; // Default file name
            dlg.DefaultExt = ".enex"; // Default file extension
            dlg.Filter = "Enex File (.enex)|*.enex"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                this.fileName = dlg.FileName;
                Browse_textBox.Text = this.fileName;
                this.noteManager.notesPath = this.fileName;
            }
        }

        private void Close_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Save_button_Click(object sender, RoutedEventArgs e)
        {
            this.noteManager.generateEnex();
            Save_button.Visibility = System.Windows.Visibility.Hidden;
        }

        #region Process
        private void Process_button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += this.noteManager.generateNotes;
                worker.ProgressChanged += worker_ProgressChanged;
                worker.RunWorkerCompleted += worker_RunWorkerCompleted;

                worker.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProcessingStatus_progressBar.Visibility = System.Windows.Visibility.Hidden;
            Status_textBlock.Text = "Processing Completed";
            Process_button.Visibility = System.Windows.Visibility.Hidden;
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProcessingStatus_progressBar.Value = e.ProgressPercentage;
        }
        #endregion
    }
}

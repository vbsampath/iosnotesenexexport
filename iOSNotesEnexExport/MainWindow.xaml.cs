﻿using System;
using System.Windows;
using Microsoft.Win32;

namespace iOSNotesEnexExport
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NoteManager noteManager = new NoteManager();
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NotesPath_button_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".sqlite"; // Default file extension
            dlg.Filter = "SQLite Databases (.sqlite)|*.sqlite"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                noteManager.notesDbPath = dlg.FileName;
                NotesPath_textBox.Text = dlg.FileName;
            }
        }

        private void Load_button_Click(object sender, RoutedEventArgs e)
        {
            Notes notes = new Notes(this.noteManager);
            notes.ShowDialog();
        }

        private void Close_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

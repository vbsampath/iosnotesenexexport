﻿using System;
using System.Configuration;

namespace iOSNotesEnexExport
{
    public class Constants
    {
        #region app settings
        public static string Static_NotesDatabasePath
        {
            get
            {
                return ConfigurationManager.AppSettings["notesDatabasePath"];
            }
        }
        public static string Static_NotesQuery
        {
            get
            {
                return ConfigurationManager.AppSettings["notesSQLiteQuery"];
            }
        }
        public static string Static_Version
        {
            get
            {
                return ConfigurationManager.AppSettings["version"];
            }
        }
        public static string Static_Application
        {
            get
            {
                return ConfigurationManager.AppSettings["application"];
            }
        }
        public static string Static_DateFormat
        {
            get
            {
                return ConfigurationManager.AppSettings["dateformat"];
            }
        }
        #endregion
    }
}

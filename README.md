# README #

This tool tries to export ios notes to enex format which in turn can be importing into evernote using its windows software.

For more than 6 months I have been looking over internet for a viable solution to export my ios notes to evernote with all bells and whistles like getting created/modified time correct because when exporting they are important otherwise why do we need backups when we can't restore them chronologically.

So I used my limited skills to write up a simple program to do just that and its no where near perfect. If you find any errors please post a issue.


### Prerequisites ###

* .Net 4 Client profile runtime
* Windows 7 or higher
* jailbroken ios
* notes.sqlite (learn about its location on the internet)
* Evernote windows program


### What is this repository for? ###

This repository was created just out of need because there is not much help available on internet to export my ios notes to evernote.

This code is work in progress and logically complete for my requirement because I got my ios notes 

### How do I get set up? ###

1. Goto this url https://bitbucket.org/vbsampath/iosnotesenexexport/downloads
2. Download Setup.exe and iOSNotesEnexExport.application files into same folder
3. Click on the Setup.exe to start installation process


### Running the Tool ###

1. After installing open the application
2. Load up the notes.sqlite file extracted from ios filesystem
3. One can see all notes loaded up in the window and then press "Process"
4. Click "Process" bit button for processing notes
5. Browse a location to save your file (notes.enex default filename)
6. Close all the windows ( sorry I don't have much knowledge on all best practices. This was just for my use so it runs for me)
7. Open Evernote program
8. Locate where you saved notes.enex 
9. Import the enex file using evernote inbuilt import feature. 

### Uninstall ###

1. Start->Control Panel->Programs and Features
2. Find iOSNotesEnexExport and click on it
3. Uninstall

### Limitations ###
1. It was tested only on windows 7 machine and I already got every .net runtime installed
2. Tested against ios 7.0.4 jailbroken
3. Very naive coding standards so expect issues.

### Who do I talk to? ###

For any issues post in the issues section